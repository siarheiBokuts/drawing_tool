# Project Title

=)
### Installing

Just run 
```
npm install
```

And then

```
node bin/www
```

Thic command  starts node  server and  performs all steps from the test  

## File location

File location - ```FileData/elephant/input.txt```

### Some info

I've done few validations because another wasn't mentioned 
 - In the first step the canvas should be drawn (if not it  - error)   -``` /Examples/1.png```
 - Сordinates cannot be negative (except fill point)          -``` /Examples/2.png```
 - Сheck out coordinates beyond canvas (in lines and rectangles)  ``` /Examples/3.png```
 - Skip empty lines ``` /Examples/4.png```
 - Skip useless symbols ``` /Examples/5.png```
 - The sequence of points is not important (in lines and rectangles)   ``` /Examples/6.png```

### Dop info
You can see full examples in ```/Exampples/7.png```

Examples input.txt/output.txt  ```/Exampples/8.png```
## Authors

* **Siarhei Bokuts**  



## License

This project is licensed under the MIT License.



